package chapter1.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter1.beans.User;
import chapter1.beans.UserSupplement;
import chapter1.service.UserService;


@WebServlet(urlPatterns = { "/userList" })
public class UserListServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req,
			HttpServletResponse res) throws IOException, ServletException {
		List<UserSupplement> userList = new UserService().getAlluserist();

		req.setAttribute("userList", userList);

		HttpSession session = req.getSession();
		User status = (User) session.getAttribute("loginUser");
		req.setAttribute("myId", status.getId());

		req.getRequestDispatcher("userManagement.jsp").forward(req, res);
	}

	@Override
	protected void doPost(HttpServletRequest req,
			HttpServletResponse res) throws IOException, ServletException {

	}
}



