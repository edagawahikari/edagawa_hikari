package chapter1.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter1.beans.Comments;
import chapter1.beans.User;
import chapter1.service.CommentsService;

@WebServlet(urlPatterns = { "/comments" })
public class NewCommentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}



	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();

		if (isValid(request, comments) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comments comment = new Comments();
			comment.setText(request.getParameter("comments"));
			comment.setUserId(Integer.parseInt(request.getParameter("userId")));
			comment.setMessageId(Integer.parseInt(request.getParameter("messageid")));

			new CommentsService().register(comment);

			response.sendRedirect("top");
		} else {
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("top");
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("comments");

		if (StringUtils.isEmpty(comment) == true) {
			comments.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}