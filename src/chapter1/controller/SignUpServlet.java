package chapter1.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter1.beans.User;
import chapter1.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginID(request.getParameter("loginID"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(request.getParameter("branch"));
            user.setDepartment(request.getParameter("department"));

            new UserService().register(user);

            response.sendRedirect("userlist");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String loginID = request.getParameter("loginID");
        String password = request.getParameter("password");
        String branch = request.getParameter("branch");
        String department = request.getParameter("department");

        if (StringUtils.isEmpty(name) == true) {
        	messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(loginID) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(branch) == true) {
        	messages.add("支店名を入力してください");
        }
        if (StringUtils.isEmpty(department) == true) {
        	messages.add("部署・役職名を入力してください");
        }
        //アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}