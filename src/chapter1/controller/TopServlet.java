package chapter1.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter1.beans.User;
import chapter1.beans.UserComments;
import chapter1.beans.UserMessage;
import chapter1.service.CommentsService;
import chapter1.service.MessageService;

@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");


        List<UserMessage> messages = new MessageService().getMessage();

        request.setAttribute("messages", messages);

        List<UserComments> comments = new CommentsService().getComments();

        request.setAttribute("comments", comments);


        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}