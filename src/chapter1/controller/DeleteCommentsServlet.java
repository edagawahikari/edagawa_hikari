package chapter1.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter1.service.CommentsService;

@WebServlet(urlPatterns = { "/deletecomments" })
public class DeleteCommentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

   @Override
    protected void doGet(HttpServletRequest request,
    		HttpServletResponse response) throws IOException, ServletException {
	   int id = 0;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		} catch (Exception e) {
			return;
		}
		new CommentsService().delete(id);
		response.sendRedirect(request.getHeader("Referer"));

	}
}