package chapter1.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter1.beans.Message;
import chapter1.beans.User;
import chapter1.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		User status = (User)session.getAttribute("loginUser");
		request.setAttribute("userId",status.getId());
		request.getRequestDispatcher("newmessage.jsp").forward(request, response);
	}



	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setText(request.getParameter("message"));
			message.setSubject(request.getParameter("subject"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());

			new MessageService().register(message);

			response.sendRedirect("top");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("newMessage");
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		String category = request.getParameter("category");

		if(StringUtils.isEmpty(subject)) {
			messages.add("件名を入力してください");
		}else if(subject.length()>50) {
			messages.add("件名は50文字以下で入力してください");
		}
		if (StringUtils.isEmpty(message) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < message.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if(StringUtils.isEmpty(category)) {
			messages.add("カテゴリーを入力してください");
		}else if(category.length()>10) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}