package chapter1.dao;

import static chapter1.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter1.beans.UserComments;
import chapter1.exception.SQLRuntimeException;

public class UserCommentsDao {

	public List<UserComments> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("users.loginID as loginID, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_date as created_date, ");
			sql.append("comments.message_id as message_id ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComments> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComments> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserComments> ret = new ArrayList<UserComments>();
		try {
			while (rs.next()) {
				String loginID = rs.getString("loginID");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int messageId = rs.getInt("message_Id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComments comments = new UserComments();
				comments.setLoginID(loginID);
				comments.setName(name);
				comments.setId(id);
				comments.setUserId(userId);
				comments.setMessageid(messageId);
				comments.setText(text);
				comments.setCreated_date(createdDate);

				ret.add(comments);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}