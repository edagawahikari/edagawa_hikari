package chapter1.dao;

import static chapter1.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter1.beans.User;
import chapter1.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("loginID");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch");
			sql.append(", department");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // name
			sql.append(", ?"); // email
			sql.append(", ?"); // password
			sql.append(", ?"); // description
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginID());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getBranch());
			ps.setString(5, user.getDepartment());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginID,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE loginID = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginID);
			ps.setString(2, password);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			} }catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginID = rs.getString("loginID");
				String name = rs.getString("name");
				String password = rs.getString("password");
				String branch = rs.getString("branch");
				String department = rs.getString("department");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setLoginID(loginID);
				user.setName(name);
				user.setPassword(password);
				user.setBranch(branch);
				user.setDepartment(department);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	public List<User> getList (Connection connection){
		PreparedStatement ps = null;
		try {

			String sql = "SELECT * from users";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<User>ret = new ArrayList<User>();

			while(rs.next()) {
				String name = rs.getString("name");
				String loginID = rs.getString("loginiD");
				String branch = rs.getString("branch");
				String department = rs.getString("department");

				User userList = new User();
				userList.setName(name);
				userList.setLoginID(loginID);
				userList.setBranch(branch);
				userList.setDepartment(department);

				ret.add(userList);
			}
			if(ret.isEmpty()) {
				return null;
			}else {
				return ret;
			}
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close (ps);
		}
	}
}
