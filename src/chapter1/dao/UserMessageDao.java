package chapter1.dao;

import static chapter1.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter1.beans.UserMessage;
import chapter1.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.subject as subject,");
			sql.append("messages.category as category,");
			sql.append("messages.text as text, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.loginID as loginID, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginID = rs.getString("loginID");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setLoginID(loginID);
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setText(text);
				message.setSubject(subject);
				message.setCategory(category);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}