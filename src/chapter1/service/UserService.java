package chapter1.service;

import static chapter1.utils.CloseableUtil.*;
import static chapter1.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter1.beans.User;
import chapter1.dao.UserDao;
import chapter1.utils.CipherUtil;

public class UserService {

	public void register(User userList) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(userList.getPassword());
			userList.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, userList);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<User> getList() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<User>userList = userDao.getList(connection);

			commit(connection);

			return userList;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}