package chapter1.service;

import static chapter1.utils.CloseableUtil.*;
import static chapter1.utils.DBUtil.*;

import java.sql.Connection;

import chapter1.beans.User;
import chapter1.dao.UserDao;
import chapter1.utils.CipherUtil;

public class LoginService {

    public User login(String loginID, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, loginID, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}