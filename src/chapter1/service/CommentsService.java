package chapter1.service;

import static chapter1.utils.CloseableUtil.*;
import static chapter1.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter1.beans.Comments;
import chapter1.beans.UserComments;
import chapter1.dao.CommentsDao;
import chapter1.dao.UserCommentsDao;


public class CommentsService {

    public void register(Comments comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentsDao commentsDao = new CommentsDao();
            commentsDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		CommentsDao commentDao = new CommentsDao();
    		commentDao.delete(connection,id);
    		commit(connection);
    	}catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	}catch(Error e) {
    		rollback(connection);
    		throw e;
    	}finally {
    		close(connection);
    	}
    }


    private static final int LIMIT_NUM = 1000;

    public List<UserComments> getComments() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentsDao commentDao = new UserCommentsDao();
            List<UserComments> ret = commentDao.getUserComments(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}