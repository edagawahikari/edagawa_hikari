package chapter1.beans;

import java.io.Serializable;

public class UserSupplement implements Serializable  {
	private static final long serialVersionUID = 1L;

	private String name;
	private String loginID;
	private String branch;
	private String department;
	private String branchBranch;
	private String departmentDepartment;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getBranchBranch() {
		return branchBranch;
	}
	public void setBranchBranch(String branchBranch) {
		this.branchBranch = branchBranch;
	}
	public String getDepartmentDepartment() {
		return departmentDepartment;
	}
	public void setDepartmentDepartment(String departmentDepartment) {
		this.departmentDepartment = departmentDepartment;
	}

}
