package chapter1.beans;

import java.io.Serializable;
import java.util.Date;

public class Comments implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int userid;
    private int messageid;
    private String text;
    private Date createdDate;
    private Date updatedDate;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getUserId() {
		return userid;
	}
	public void setUserId(int userid) {
		this.userid = userid;
	}
	public int getMessageId() {
		return messageid;
	}
	public void setMessageId(int messageid) {
		this.messageid = messageid;
	}




}