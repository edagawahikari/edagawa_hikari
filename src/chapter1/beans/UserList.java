package chapter1.beans;

public class UserList {
	private String name;
	private String loginID;
	private String branch;
	private String department;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}

	public void User(String name, String loginID, String branch,String department) {
		this.name = name;
		this.loginID = loginID;
		this.branch = branch;
		this.department = department;
	}


}
