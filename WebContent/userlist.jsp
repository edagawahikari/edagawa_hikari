<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="signup">ユーザー登録</a>
			</c:if>
		</div>
	</div>
	<br>

	<div class="userlist">
		<table border=1>
			<captation>《ユーザーリスト》</captation>
			<tr bgcolor="#ffffc0">
				<th>名前</th>
				<th>ログインID</th>
				<th>部署名</th>
				<th>役職名</th>
				<th>編集</th>
				<th>停止</th>
				<c:forEach var="user" items="${userList}" varStatus="status">
					<tr>
						<td><c:out value="${status.index}" /></td>
						<td><c:out value="${user.name }" /></td>
						<td><c:out value="${user.loginid}" /></td>
						<td><c:out value="${user.branch}" /></td>
						<td><c:out value="${user.department}" /></td>
				</c:forEach>
		</table>
	</div>
</body>
</html>