<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body bgcolor = "lightgray">
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="logout">ログアウト</a>
				<a href="newMessage">新規投稿</a>
				<a href="userList">ユーザー管理</a>
			</c:if>
		</div>

		<div style="text-align: center">
			<h1>
				<u>掲示板</u>
			</h1>
			<font size="18"></font>
		</div>
		<br>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="loginID">
					@
					<c:out value="${loginUser.loginID}" />
				</div>
				<div class="loginID"></div>
			</div>
		</c:if>
	</div>
	<div class="copyright">Copyright(c)YourName</div>
	<br>



	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="name">
					<c:out value="${message.name}" />
				</div>
				<div class="subject">
					【件名】
					<br><c:out value="${message.subject}" />
				</div>
				<div class="text">
					【本文】
					<br><c:out value="${message.text}" />
				</div>
				<div class="category">
					【カテゴリ】
					<br><c:out value="${message.category}" />
				</div>
				<div class="date">
					<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
			</div>

			<form action="comments" method="post">
				<br>《コメント》<br /> <input name="messageid" value="${message.id}"
					type="hidden">

				<textarea name="comments" cols="100" rows="5" class="tweet-box"></textarea>
				<br /> <input type="submit" value="投稿">（500文字まで）<br>

			</form>


			<div class="comments">
				<c:forEach items="${comments}" var="comment">
					<c:if test="${ message.id==comment.messageid }">
						<div class="comment">
							<br><div class="name">
								<c:out value="${comment.name}" />
							</div>
							<div class="text">
								<c:out value="${comment.text}" />
							</div>
							<div class="date">
								<fmt:formatDate value="${comment.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<input type="submit" value="削除"><br>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<br>
			<hr>
			<br>
		</c:forEach>
	</div>
</body>
</html>
