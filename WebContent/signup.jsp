<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br />


			<p><label for="name">〈名前〉</label>
			<br><input name="name" id="name" /><br /></p>


			<p><label for="loginID">〈ログインID〉</label>
			<br><input name="loginID" id="loginID" /> <br /></p>


			<p><label for="password">〈パスワード〉</label>
			<br><input name="password" type="password" id="password" /> <br /></p>

			<p><label for="branch">〈支店名〉</label>
			<br><input name="branch" id="branch" /> <br /></p>

			<p><label for="department">〈部署・役職名〉</label>
			<br><input name="department" id="department" /> <br /></p>

			<br><input type="submit" value="登録" class="btn btn-default" />
			<br /> <a href="./">戻る</a>



		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>